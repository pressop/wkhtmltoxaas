/// <reference types="node" />
import { Command, CommandContext, CommandRequest } from 'comaas';
import { ServerResponse } from 'http';
import { Observable } from 'rxjs/Observable';
export declare abstract class WkHTMLToX implements Command {
    private bin;
    private path;
    private static TMP_DIR;
    protected constructor(bin: string, path: string);
    getBin(): string;
    getPath(): string;
    createContext(request: CommandRequest): Observable<CommandContext>;
    prepareResponse(response: ServerResponse, request: CommandRequest): void;
    abstract getFormat(request: CommandRequest): string;
    abstract getMimeType(request: CommandRequest): string;
}
