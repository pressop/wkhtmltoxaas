"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var wkhtmltox_1 = require("./wkhtmltox");
var WkHTMLToPDF = /** @class */ (function (_super) {
    __extends(WkHTMLToPDF, _super);
    function WkHTMLToPDF(bin) {
        if (bin === void 0) { bin = '/usr/local/bin/wkhtmltopdf'; }
        return _super.call(this, bin, '/wkhtmltopdf') || this;
    }
    ;
    WkHTMLToPDF.prototype.getFormat = function (request) {
        return 'pdf';
    };
    WkHTMLToPDF.prototype.getMimeType = function (request) {
        return 'application/pdf';
    };
    return WkHTMLToPDF;
}(wkhtmltox_1.WkHTMLToX));
exports.WkHTMLToPDF = WkHTMLToPDF;
