"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var fs_1 = require("fs");
var Ajv = require("ajv");
var Config = /** @class */ (function () {
    function Config(schemaFile, configFile) {
        schemaFile = path_1.resolve(__dirname, schemaFile);
        var ajv = new Ajv({
            useDefaults: true
        });
        this.configSchema = JSON.parse(fs_1.readFileSync(schemaFile, 'utf-8'));
        this.validate = ajv.compile(this.configSchema);
        this.configFile = path_1.resolve(__dirname, configFile);
        this.loadFile();
    }
    Config.prototype.loadFile = function () {
        this._config = JSON.parse(fs_1.readFileSync(this.configFile, 'utf-8'));
        if (!this.validate(this._config)) {
            throw new Error(JSON.stringify(this.validate.errors));
        }
    };
    Object.defineProperty(Config.prototype, "config", {
        get: function () {
            return this._config;
        },
        enumerable: true,
        configurable: true
    });
    return Config;
}());
exports.Config = Config;
