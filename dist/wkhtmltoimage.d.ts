import { CommandRequest } from 'comaas';
import { WkHTMLToX } from './wkhtmltox';
export declare class WkHTMLToImage extends WkHTMLToX {
    constructor(bin?: string);
    getFormat(request: CommandRequest): string;
    getMimeType(request: CommandRequest): string;
}
