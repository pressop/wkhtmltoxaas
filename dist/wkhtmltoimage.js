"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var wkhtmltox_1 = require("./wkhtmltox");
var WkHTMLToImage = /** @class */ (function (_super) {
    __extends(WkHTMLToImage, _super);
    function WkHTMLToImage(bin) {
        if (bin === void 0) { bin = '/usr/local/bin/wkhtmltoimage'; }
        return _super.call(this, bin, '/wkhtmltoimage') || this;
    }
    ;
    WkHTMLToImage.prototype.getFormat = function (request) {
        var format = null;
        if (request.hasOwnProperty('args')) {
            for (var _i = 0, _a = request.args; _i < _a.length; _i++) {
                var arg = _a[_i];
                if ('--format' === arg.key) {
                    format = String(arg.value);
                    break;
                }
            }
        }
        if (null === format) {
            format = 'jpg';
        }
        return format;
    };
    WkHTMLToImage.prototype.getMimeType = function (request) {
        var format = this.getFormat(request);
        if ('png' === format) {
            return 'image/png';
        }
        return 'image/jpeg';
    };
    return WkHTMLToImage;
}(wkhtmltox_1.WkHTMLToX));
exports.WkHTMLToImage = WkHTMLToImage;
