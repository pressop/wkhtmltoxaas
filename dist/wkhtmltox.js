"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var comaas_1 = require("comaas");
var Observable_1 = require("rxjs/Observable");
var os_1 = require("os");
var crypto_1 = require("crypto");
var fs_1 = require("fs");
var path_1 = require("path");
var WkHTMLToX = /** @class */ (function () {
    function WkHTMLToX(bin, path) {
        this.bin = bin;
        this.path = path;
    }
    ;
    WkHTMLToX.prototype.getBin = function () {
        return this.bin;
    };
    WkHTMLToX.prototype.getPath = function () {
        return this.path;
    };
    WkHTMLToX.prototype.createContext = function (request) {
        var _this = this;
        return Observable_1.Observable.create(function (observer) {
            var context = new comaas_1.CommandContext();
            var args = Object.assign([], request.args);
            var filename = WkHTMLToX.TMP_DIR + path_1.sep + crypto_1.createHash('md5')
                .update(Date.now().toString()).digest('hex')
                + '.' + _this.getFormat(request);
            args.push({
                key: filename,
            });
            context.cmd = _this.getBin();
            context.args = comaas_1.ArgumentParser.normalize(args);
            context.before_exec = Observable_1.Observable.create(function (beforeObserver) {
                fs_1.mkdir(WkHTMLToX.TMP_DIR, function (error) {
                    if (null !== error && -4075 !== error.errno) {
                        beforeObserver.error(new comaas_1.HttpError(error.message, 503));
                    }
                    else {
                        beforeObserver.next(context);
                    }
                    beforeObserver.complete();
                });
            });
            context.after_exec = Observable_1.Observable.create(function (afterObserver) {
                fs_1.readFile(filename, function (error, data) {
                    if (null === error) {
                        fs_1.unlink(filename, function (error) {
                            if (null === error) {
                                context.output = data;
                                afterObserver.next(context);
                            }
                            else {
                                afterObserver.error(new comaas_1.HttpError(error.message, 503));
                            }
                            afterObserver.complete();
                        });
                    }
                    else {
                        afterObserver.error(new comaas_1.HttpError(error.message, 503));
                        afterObserver.complete();
                    }
                });
            });
            observer.next(context);
            observer.complete();
        });
    };
    WkHTMLToX.prototype.prepareResponse = function (response, request) {
        response.statusCode = 200;
        response.setHeader('Content-Type', this.getMimeType(request));
    };
    WkHTMLToX.TMP_DIR = os_1.tmpdir() + path_1.sep + 'wkhtmltox';
    return WkHTMLToX;
}());
exports.WkHTMLToX = WkHTMLToX;
