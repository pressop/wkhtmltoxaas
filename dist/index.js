"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var comaas_1 = require("comaas");
var path_1 = require("path");
var fs_1 = require("fs");
var wkhtmltopdf_1 = require("./wkhtmltopdf");
var config_1 = require("./config");
var wkhtmltoimage_1 = require("./wkhtmltoimage");
var configPath = process.cwd() + path_1.sep + 'config.json';
fs_1.stat(configPath, function (error, stats) {
    var commands = [];
    if (null === error && stats.isFile()) {
        var config = new config_1.Config('../schema.json', configPath);
        commands.push(new wkhtmltopdf_1.WkHTMLToPDF('"' + config.config.wkhtmltopdf + '"'));
        commands.push(new wkhtmltoimage_1.WkHTMLToImage('"' + config.config.wkhtmltoimage + '"'));
    }
    else {
        commands.push(new wkhtmltopdf_1.WkHTMLToPDF());
        commands.push(new wkhtmltoimage_1.WkHTMLToImage());
    }
    var server = comaas_1.createServer(commands);
    server.listen(8156, '127.0.0.1', function () {
        console.log('Serveur lancé sur http://127.0.0.1:8156');
    });
});
