export interface ConfigInterface {
    wkhtmltopdf: string;
    wkhtmltoimage: string;
}
export declare class Config {
    private readonly configSchema;
    private readonly validate;
    private readonly configFile;
    private _config;
    constructor(schemaFile: string, configFile: string);
    private loadFile;
    readonly config: ConfigInterface;
}
