WkHTMLtoX as a Service
==================

Make WkHTMLtoPDF and WkHTMLtoImage command available as http service. Useful in micro services.

## Install
```bash
yarn add wkhtmltox
```

## Run
```bash
yarn server
```

Request

```bash
curl --request POST \
  --url http://127.0.0.1:8156/wkhtmltopdf \
  --header 'content-type: application/json' \
  --data '{
    "args": [
      "https://www.google.com"
    ]
}'

curl --request POST \
  --url http://127.0.0.1:8156/wkhtmltoimage \
  --header 'content-type: application/json' \
  --data '{
    "args":[{
        "key": "--format",
        "value": "png"
      },
      "https://www.google.com"
    ]
}'
```
 
## Know issues
- Nothing (yet)

## License

Release under MIT license.
