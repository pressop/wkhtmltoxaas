import {CommandRequest} from 'comaas';
import {WkHTMLToX} from './wkhtmltox';

export class WkHTMLToImage extends WkHTMLToX {
    constructor(bin: string = '/usr/local/bin/wkhtmltoimage') {
        super(bin, '/wkhtmltoimage');
    };

    getFormat(request: CommandRequest): string {
        let format: string = null;
        if (request.hasOwnProperty('args')) {
            for (const arg of request.args) {
                if ('--format' === arg.key) {
                    format = String(arg.value);
                    break;
                }
            }
        }

        if (null === format) {
            format = 'jpg';
        }

        return format;
    }

    getMimeType(request: CommandRequest): string {
        const format = this.getFormat(request);

        if ('png' === format) {
            return 'image/png';
        }

        return 'image/jpeg';
    }
}
