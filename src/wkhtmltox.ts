import {Argument, ArgumentParser, Command, CommandContext, CommandRequest, HttpError} from 'comaas';
import {ServerResponse} from 'http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {tmpdir} from 'os';
import {createHash} from 'crypto';
import {mkdir, readFile, unlink} from 'fs';
import {sep} from 'path';

export abstract class WkHTMLToX implements Command {
    private static TMP_DIR = tmpdir() + sep + 'wkhtmltox';

    protected constructor(private bin: string, private path: string) {};

    getBin(): string {
        return this.bin;
    }

    getPath(): string {
        return this.path;
    }

    createContext(request: CommandRequest): Observable<CommandContext> {
        return Observable.create((observer: Observer<CommandContext>) => {
            const context = new CommandContext();
            const args: Array<Argument> = Object.assign([], request.args);
            const filename = WkHTMLToX.TMP_DIR + sep + createHash('md5')
                    .update(Date.now().toString()).digest('hex')
                + '.' + this.getFormat(request)
            ;

            args.push(<Argument>{
                key: filename,
            });

            context.cmd = this.getBin();
            context.args = ArgumentParser.normalize(args);

            context.before_exec = Observable.create((beforeObserver: Observer<CommandContext>) => {
                mkdir(WkHTMLToX.TMP_DIR, (error: NodeJS.ErrnoException|null) => {
                    if (null !== error && -4075 !== error.errno) {
                        beforeObserver.error(new HttpError(error.message, 503));
                    } else {
                        beforeObserver.next(context);
                    }

                    beforeObserver.complete();
                });
            });

            context.after_exec = Observable.create((afterObserver: Observer<CommandContext>) => {
                readFile(filename, (error: NodeJS.ErrnoException, data: Buffer) => {
                    if (null === error) {
                        unlink(filename, (error: NodeJS.ErrnoException) => {
                            if (null === error) {
                                context.output = data;
                                afterObserver.next(context);
                            } else {
                                afterObserver.error(new HttpError(error.message, 503));
                            }

                            afterObserver.complete();
                        });
                    } else {
                        afterObserver.error(new HttpError(error.message, 503));
                        afterObserver.complete();
                    }
                });
            });

            observer.next(context);
            observer.complete();
        });
    }

    prepareResponse(response: ServerResponse, request: CommandRequest): void {
        response.statusCode = 200;
        response.setHeader('Content-Type', this.getMimeType(request));
    }

    abstract getFormat(request: CommandRequest): string;
    abstract getMimeType(request: CommandRequest): string;
}
