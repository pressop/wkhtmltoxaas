import {resolve} from 'path';
import {readFileSync} from 'fs';
import * as Ajv from 'ajv';
import {ValidateFunction} from 'ajv';

export interface ConfigInterface {
    wkhtmltopdf: string;
    wkhtmltoimage: string;
}

export class Config {
    private readonly configSchema: object;
    private readonly validate: ValidateFunction;
    private readonly configFile: string;
    private _config: ConfigInterface;

    constructor(schemaFile: string, configFile: string) {
        schemaFile = resolve(__dirname, schemaFile);
        let ajv = new Ajv({
            useDefaults: true
        });

        this.configSchema = JSON.parse(readFileSync(schemaFile, 'utf-8'));
        this.validate = ajv.compile(this.configSchema);
        this.configFile = resolve(__dirname, configFile);

        this.loadFile();
    }

    private loadFile(): void {
        this._config = JSON.parse(readFileSync(this.configFile, 'utf-8'));

        if (!this.validate(this._config)) {
            throw new Error(JSON.stringify(this.validate.errors));
        }
    }

    get config(): ConfigInterface {
        return this._config;
    }
}
