import {CommandRequest} from 'comaas';
import {WkHTMLToX} from './wkhtmltox';

export class WkHTMLToPDF extends WkHTMLToX {
    constructor(bin: string = '/usr/local/bin/wkhtmltopdf') {
        super(bin, '/wkhtmltopdf');
    };

    getFormat(request: CommandRequest): string {
        return 'pdf';
    }

    getMimeType(request: CommandRequest): string {
        return 'application/pdf';
    }
}
