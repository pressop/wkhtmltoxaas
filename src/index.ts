import {Command, createServer} from 'comaas';
import {sep} from 'path';
import {stat, Stats} from 'fs';
import {WkHTMLToPDF} from './wkhtmltopdf';
import {Config} from './config';
import {WkHTMLToImage} from './wkhtmltoimage';

const configPath = process.cwd() + sep + 'config.json';

stat(configPath, (error: NodeJS.ErrnoException, stats: Stats) => {
    let commands: Array<Command> = [];

    if (null === error && stats.isFile()) {
        const config = new Config('../schema.json', configPath);

        commands.push(new WkHTMLToPDF('"' + config.config.wkhtmltopdf + '"'));
        commands.push(new WkHTMLToImage('"' + config.config.wkhtmltoimage + '"'));
    } else {
        commands.push(new WkHTMLToPDF());
        commands.push(new WkHTMLToImage());
    }

    let server = createServer(commands);

    server.listen(8156, '127.0.0.1', () => {
        console.log('Serveur lancé sur http://127.0.0.1:8156');
    });
});
